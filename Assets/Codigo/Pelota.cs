﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pelota : MonoBehaviour 
{
	public float velocidadInicial = 600f;
	Rigidbody rig;
	bool enJuego;

	Vector3 posicionInicial;

	Transform barra;

	void Awake ()
	{
		rig = GetComponent<Rigidbody>();
		barra = GetComponentInParent<Transform>();
	}

	// Use this for initialization
	void Start () 
	{
		posicionInicial = transform.position;
	}

	public void Reset()
    {
        PonerPelotaSobreBarra();
        enJuego = false;
        DetenerMovimiento();
    }

    public void DetenerMovimiento()
	{
		rig.isKinematic = true;
		rig.velocity = Vector3.zero;
	}
	
	private void PonerPelotaSobreBarra()
    {
        transform.position = posicionInicial;
        transform.SetParent(barra);
    }
	
	// Update is called once per frame
	void Update ()
	{
		if (!enJuego && Input.GetButtonDown("Fire1"))
		{
			enJuego = true;
			transform.SetParent(null);
			rig.isKinematic = false;
			rig.AddForce(new Vector3(velocidadInicial, velocidadInicial, 0));
		}
	}
}